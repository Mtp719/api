# 物品

#### drop
请求丢弃物品

* 参数
    * item (skill) - 物品
    * target (drop) - 丢弃位置

```lua
ac.item.drop(item, target)
```
