# 设置

#### hide
隐藏设置界面

* 返回
    * valid (boolean) - 是否合法

```lua
local res = ac.setting:hide()
```

#### show
显示设置界面

* 返回
    * valid (boolean) - 是否合法

```lua
local res = ac.setting:show()
```

#### is_visible
是否显示

* 返回
    * visible (boolean) - 是否显示

```lua
local visible = ac.setting:is_visible()
```
