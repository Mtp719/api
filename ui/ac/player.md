# 玩家

#### 获取
* 参数
    * id (integer) - 玩家ID
* 返回
    * player (player) - 玩家

```lua
local player = ac.player(id)
```

#### 本地玩家
* 返回
    * player (player) - 玩家

```lua
local player = ac.local_player()
```

#### controller
获取玩家控制者

* 返回
    * controller (string) - 会是以下值中的一个
        + `human` - 用户
        + `none` - 空位
        + `computer` - 电脑
        + `ai` - AI

```lua
local controller = player:controller()
```

#### each_player
遍历玩家

* 参数
    * *type* (string) - 玩家类型
* 返回
    * player (player) - 玩家

当设置了`type`后，会遍历所有该类型的玩家（根据[config]中的玩家定义）；否则会遍历所有玩家。按照玩家ID从小到大的顺序遍历。

```lua
for player in ac.each_player 'user' do
    -- 你的代码
end
```

#### event
注册事件

* 参数
    * name (string) - 事件名
    * callback (function) - 事件函数
* 返回
    * trigger (trigger) - 触发器
* 事件参数
    * trigger (trigger) - 触发器
    * ... (...) - 自定义数据

这是对`ac.event_register`方法的封装，你可以在[这里][event]看到详细说明。

```lua
local trigger = player:event('玩家-选择英雄', function (trigger, player, name)
    -- 你的代码
end)
```

#### event_dispatch
触发事件

* 参数
    * name (string) - 事件名
    * ... (...) - 自定义数据

这是对`ac.event_dispatch`方法的封装，你可以在[这里][event]看到详细说明。

```lua
player:event_dispatch('自定义事件', ...)
```

#### event_notify
触发事件

* 参数
    * name (string) - 事件名
    * ... (...) - 自定义数据

这是对`ac.event_notify`方法的封装，你可以在[这里][event]看到详细说明。

```lua
player:event_notify('自定义事件', ...)
```

#### game_state
获取游戏状态

* 返回
    * state (string) - 会是以下值中的一个
        + `none` - 空位
        + `online` - 在线
        + `offline` - 离线

```lua
local state = player:game_state()
```

#### get
获取属性

* 参数
    * state (string) - 属性名称
* 返回
    * value (number) - 数值

玩家属性说明见[这里][玩家属性]

```lua
local value = player:get '金钱'
```

#### get_hero
获取英雄

* 返回
    * hero (unit) - [玩家的英雄]

如果玩家没有英雄，则返回`nil`。

```lua
local hero = player:get_hero()
```

#### get_hero_name
获取英雄名字

* 返回
    * name (string) - [玩家的英雄]的名字

如果玩家没有英雄，则返回空字符串。

```lua
local name = player:get_hero_name()
```

#### get_slot_id
获取槽位ID

* 返回
    * id (integer) - 槽位ID

```lua
local id = player:get_slot_id()
```

#### get_team_id
获取队伍ID

* 返回
    * id (integer) - 队伍ID

```lua
local id = player:get_team_id()
```

#### user_name
获取用户名

* 返回
    * name (string) - 用户名

若该玩家是空位，则返回空字符串。

```lua
local name = player:user_name()
```

#### user_title
获取用户称号

* 返回
    * title (string) - 用户的称号

若该玩家是空位，则返回空字符串。

```lua
local title = player:user_title()
```

[玩家的英雄]: http://192.168.1.19/server/#/ac/player/玩家的英雄
[玩家属性]: http://192.168.1.19/server/#/ac/player/attribute
[event]: /ac/api/event
[config]: 404
