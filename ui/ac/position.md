# 位置
使用`(X, Y)`的形式描述画面上的一个位置。描述时，以屏幕左上角为`(0, 0)`，X轴向右为正，Y轴向下为正。

#### 创建
* 参数
    * x (integer) - X坐标
    * y (integer) - Y坐标
* 返回
    * position (position) - 位置

```lua
local position = ac.position(x, y)
```

#### get_point
获取点

* 返回
    * point (point) - 点

获取画面位置对应的点。

```lua
local point = position:get_point()
```

#### get_xy
获取坐标

* 返回
    * x (integer) - X坐标
    * y (integer) - Y坐标

```lua
local x, y = position:get_xy()
```
