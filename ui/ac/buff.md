# 状态

#### get_name
获取名字

* 返回
    + name (string) - 状态名

```lua
local name = buff:get_name()
```

#### get_remaining
获取剩余时间

* 返回
    * time (number) - 状态的剩余持续时间（秒）

```lua
local time = buff:get_remaining()
```

#### get_time
获取总时间

* 返回
    * time (number) - 状态的总持续时间（秒）

```lua
local time = buff:get_time()
```

#### get_owner
获取拥有者

* 返回
    * owner (unit) - 状态拥有者

```lua
local owner = buff:get_owner()
```

#### get_stack
获取层数

* 返回
    * stack (integer) - 状态层数

```lua
local stack = buff:get_stack()
```

#### event
注册事件

* 参数
    * name (string) - 事件名
    * callback (function) - 事件函数
* 返回
    * trigger (trigger) - 触发器
* 事件参数
    * trigger (trigger) - 触发器
    * ... (...) - 自定义数据

这是对`ac.event_register`方法的封装，你可以在[这里][event]看到详细说明。

```lua
local trigger = buff:event('状态-改变层数', function (trigger, buff, key, value)
    -- 你的代码
end)
```

#### event_notify
触发事件

* 参数
    * name (string) - 事件名
    * ... (...) - 自定义数据

这是对`ac.event_notify`方法的封装，你可以在[这里][event]看到详细说明。

```lua
buff:event_notify('自定义事件', ...)
```
