# 模板
不同的模板拥有一些独特的功能，他们是组成界面的单元。模板的属性可以在[这里](/ui/attribute)找到。

#### button
按钮

鼠标悬停或按下时可以改变图片。

> 支持的独特属性

* [hover_image](/ui/attribute?id=hover_image)
* [active_image](/ui/attribute?id=active_image)

```lua
local button = ac.ui.button {
    image = '按钮.png',
    hover_image = '悬停.png',
    active_image = '按下.png',
}
```

#### label
标签

用于显示文本或图片。

> 支持的独特属性

* [text](/ui/attribute?id=text)
* [font](/ui/attribute?id=font)

```lua
local label = ac.ui.label {
    text = '物品名称',
    font = {
        size = 10,
        color = '#fff',
    },
}
```

#### panel
面板

最基础的容器模板，只有这个类型的模板才能添加子模板，以及设置阵列属性。

> 支持的独特属性

* [array](/ui/attribute?id=array)

```lua
local panel = ac.ui.panel {
    array = 5,
}
```

#### progress
进度条

可以按照制定百分比来显示部分图片。

> 支持的独特属性

* [progress](/ui/attribute?id=progress)
* [progress_type](/ui/attribute?id=progress_type)

```lua
local progress = ac.ui.progress {
    progress = 0.5,
    progress_type = 'left',
}
```
