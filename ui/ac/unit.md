# 单位
单位的说明见[这里][单位]。

#### each_buff
遍历状态

* 参数
    * *name* (string) - 状态名
* 遍历
    * buff (buff) - 遍历到的状态

当指定了`name`后只会遍历到该名称的状态，否则遍历所有状态。

```lua
for buff in unit:each_buff '状态名' do
    -- buff为遍历到的状态
end
```

#### each_skill
遍历技能

* 参数
    * *type* (string) - [技能类型]
* 遍历
    * skill (skill) - 遍历到的技能

如果指定了`type`，则会遍历到所有技能类型为`type`的技能；否则会遍历到所有技能。总是可以遍历到0级技能。

```lua
for skill in unit:each_skill '英雄' do
    -- skill为遍历到的技能
end
```

#### event
注册事件

* 参数
    * name (string) - 事件名
    * callback (function) - 事件函数
* 返回
    * trigger (trigger) - 触发器
* 事件参数
    * trigger (trigger) - 触发器
    * ... (...) - 自定义数据

这是对`ac.event_register`方法的封装，你可以在[这里][event]看到详细说明。

```lua
local trigger = unit:event('单位-离开视野', function (trigger, unit)
    -- 你的代码
end)
```

#### event_notify
触发事件

* 参数
    * name (string) - 事件名
    * ... (...) - 自定义数据

这是对`ac.event_notify`方法的封装，你可以在[这里][event]看到详细说明。

```lua
unit:event_notify('自定义事件', ...)
```

#### find_skill
寻找技能

* 参数
    * name (string/integer) - 技能名/格子
    * *type* (string) - [技能类型]
* 返回
    * *skill* (skill) - 技能

指定`type`后只在此类型中寻找技能。如果`name`使用格子，那么必须要指定`type`。可以找到0级技能。

```lua
local skill = unit:find_skill('技能名')
local skill = unit:find_skill(0, '英雄')
```

#### get
获取属性

* 参数
    * state (string) - [单位属性]
* 返回
    * value (number) - 数值

客户端并不一定能获取到单位的真实属性，这取决于单位是否可以见以及这个属性的[同步方式]。

```lua
local value = unit:get '生命'
```

#### get_class
获取类别

* 返回
    * class (string) - [单位类别]

如果并不知道这个单位的类别是什么，则返回`"未知"`。

```lua
local class = unit:get_class()
```

#### get_data
获取数据

* 返回
    * data (table) - 数据表

获取这个单位在[UnitData]中对应的数据表。等价于`ac.table.unit[unit:get_name()]`，因此如果单位名不正确（通常是因为不知道单位名是什么）则会返回`nil`。

#### get_facing
获取朝向

* 返回
    * facing (number) - 朝向

如果单位不可见，则返回`0.0`。

```lua
local facing = unit:get_facing()
```

#### get_height
获取高度

* 返回
    * height (number) - 高度

如果单位不可见，则返回`0.0`。

```lua
local height = unit:get_height()
```

#### get_name
获取名字

* 返回
    * name (string) - 单位名

如果并不知道这个单位的名字是什么，则会返回空字符串。

```lua
local name = unit:get_name()
```

#### get_owner
获取所有者

* 返回
    * owner (player) - 所有者

如果并不知道这个单位的所有者是谁，则返回`nil`。

```lua
local owner = unit:get_owner()
```

#### get_point
获取位置

* 返回
    * point (point) - 点

如果单位不可见，则点为`(0.0, 0.0, 0.0)`。

```lua
local point = unit:get_point()
```

#### get_type
获取类型

* 返回
    * type (string) - [单位类型]

如果并不知道这个单位的类型是什么，则返回`"未知"`。

```lua
local type = unit:get_type()
```

#### get_xy
获取坐标

* 返回
    * x (number) - X坐标
    * y (number) - Y坐标

如果单位不可见，则返回`0.0, 0.0`。

```lua
local x, y = unit:get_xy()
```

#### is_visible
是否可见

* 返回
    * result (boolean) - 是否可见

单位由不可见变为可见时触发[单位-进入视野]事件；由可见变为不可见时触发[单位-离开视野]事件。

```lua
local res = unit:is_visible()
```

[单位]: http://192.168.1.19/server/#/ac/unit/
[单位属性]: http://192.168.1.19/server/#/ac/unit/attribute
[同步方式]: http://192.168.1.19/server/#/ac/game/同步方式
[行为限制]: http://192.168.1.19/server/#/ac/unit/restriction
[技能类型]: http://192.168.1.19/server/#/ac/skill/技能类型
[单位类别]: http://192.168.1.19/server/#/ac/unit/单位类别
[单位类型]: http://192.168.1.19/server/#/ac/unit/单位类型
[单位-进入视野]: /ac/event?id=单位-进入视野
[单位-离开视野]: /ac/event?id=单位-进入视野
[UnitData]: 404
[event]: /ac/event
