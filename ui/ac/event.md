# 事件

事件其实是包含“事件”与“[触发器]”2个部分。你可以在任意对象上以某个事件名注册触发器，当该对象以该事件发起出事件时，便会被触发器接收到。发起事件时可以传入N个自定义参数，这些参数会被触发器接收到。你可以给同一个对象的某个事件名下注册多个触发器，当发起事件时，这些触发器会按照注册顺序反序执行。在这些触发器依次执行的过程中，删除一个还在排队等待执行的触发器，那么它将不会执行；新建该事件名的触发器，那么它本次也不会执行。

```lua
-- 发起一个事件，自定义参数为...
ac.event_notify(obj, name, ...)

-- 也可以用这个方法来发起事件，区别之后再讲
ac.event_dispatch(obj, name, ...)

-- 这个事件会被该触发器接收到
ac.event_register(obj, name, function (trigger, ...)
    -- 接收到的自定义参数为...
end)
```

为了方便起见，我们将一些常用对象做了封装

> 全局事件。

```lua
-- 发起
ac.game:event_notify(name, ...)
ac.game:event_dispatch(name, ...)

-- 接收
ac.game:event(name, function (trigger, ...)
end)
```

> 技能事件，技能事件发起完毕后会使用同样的参数再发起一次全局事件。

```lua
-- 发起
skill:event_notify(name, ...)
skill:event_dispatch(name, ...)

-- 接收
skill:event(name, function (trigger, ...)
end)
ac.game:event(name, function (trigger, ...)
end)
```

> 状态事件，状态事件发起完毕后会使用同样的参数再发起一次全局事件。

```lua
-- 发起
buff:event_notify(name, ...)
buff:event_dispatch(name, ...)

-- 接收
buff:event(name, function (trigger, ...)
end)
ac.game:event(name, function (trigger, ...)
end)
```

> 玩家事件，玩家事件发起完毕后会使用同样的参数再发起一次全局事件。

```lua
-- 发起
player:event_notify(name, ...)
player:event_dispatch(name, ...)

-- 接收
player:event(name, function (trigger, ...)
end)
ac.game:event(name, function (trigger, ...)
end)
```

> 单位事件，玩家事件发起完毕后会使用同样的参数再发起一次玩家事件和一次全局事件，发起玩家事件时玩家为单位的[控制者]。

```lua
-- 发起
unit:event_notify(name, ...)
unit:event_dispatch(name, ...)

-- 接收
unit:event(name, function (trigger, ...)
end)
unit:get_owner():event(name, function (trigger, ...)
end)
ac.game:event(name, function (trigger, ...)
end)
```

[event_notify]与[event_dispatch]的区别在于，`event_dispatch`可以获取到触发器的返回值：当接收事件的触发器返回了一个非`nil`的值后，当前事件将被终止，并将返回值返回给`event_dispatch`的调用方。而`event_notify`的事件则无法被终止，也不关心返回值。

```lua
ac.game:event('加法', function (trigger, a, b)
    return a + b
end)

local result = ac.game:event_dispatch('加法', 1, 2)
print(result) --> 3
```

### 方法

#### event_register
注册事件

* 参数
    * object (table/userdata) - 对象
    * name (string) - 事件名
    * callback (function) - 回调函数
* 返回
    * trigger (trigger) - 触发器对象
* 回调参数
    * trigger (trigger) - 触发器对象
    * *...* (...) - 自定义数据
* 回调返回
    * *...* (...) - 自定义返回值（只有使用[event_dispatch]时有意义）

#### event_dispatch
发起事件（关心返回值）

* 参数
    * name (string) - 事件名
    * obj (table/userdata) - 对象
    * *...* (...) - 自定义数据

#### event_notify
发起事件（不关心返回值）

* 参数
    * name (string) - 事件名
    * obj (table/userdata) - 对象
    * *...* (...) - 自定义数据

### 内置事件
幻想编辑器内置了许多事件，有些事件是通过[event_dispatch]发起的，意味着你可以给他设置返回值，这些事件会特别说明。

#### 单位-进入视野

* 参数
    + unit (unit) - 进入视野的单位

```lua
unit:event('单位-进入视野', function (trg, unit)
end)
```

#### 单位-离开视野

* 参数
    + unit (unit) - 离开视野的单位

```lua
unit:event('单位-离开视野', function (trg, unit)
end)
```

#### 单位-选中

* 参数
    + unit (unit) - 选中的单位

```lua
unit:event('单位-选中', function (trg, unit)
end)
```

#### 单位-取消选中

* 参数
    + unit (unit) - 取消选中的单位

```lua
unit:event('单位-取消选中', function (trg, unit)
end)
```

#### 单位-属性变化

* 参数
    + unit (unit) - 单位
    + key (string) - [单位属性]
    + value (number) - 属性值

```lua
unit:event('单位-属性变化', function (trg, unit, key, value)
end)
```

#### 单位-施法开始

* 参数
    + unit (unit) - 单位
    + name (string) - 技能名
    + time (number) - 已经经过的时间（毫秒）
    + total (number) - 施法开始总时间（毫秒）

单位不一定会拥有名为`name`的技能。`time`大于0说明这个单位先在视野外开始施法，让后才进入视野。

```lua
unit:event('技能-施法开始', function (trg, unit, name, time, total)
end)
```

#### 单位-施法引导

* 参数
    + unit (unit) - 单位
    + name (string) - 技能名
    + time (number) - 已经经过的时间（毫秒）
    + total (number) - 施法引导总时间（毫秒）

单位不一定会拥有名为`name`的技能。`time`大于0说明这个单位先在视野外开始施法，让后才进入视野。

```lua
unit:event('技能-施法引导', function (trg, unit, name, time, total)
end)
```

#### 单位-施法出手

* 参数
    + unit (unit) - 单位
    + name (string) - 技能名
    + time (number) - 已经经过的时间（毫秒）
    + total (number) - 施法出手总时间（毫秒）

单位不一定会拥有名为`name`的技能。`time`大于0说明这个单位先在视野外开始施法，让后才进入视野。

```lua
unit:event('技能-施法出手', function (trg, unit, name, time, total)
end)
```

#### 单位-施法停止

* 参数
    + unit (unit) - 单位
    + name (string) - 技能名

单位不一定会拥有名为`name`的技能。

```lua
unit:event('技能-施法停止', function (trg, unit, name)
end)
```

#### 技能-获得

* 参数
    + unit (unit) - 单位
    + skill (skill) - 获得的技能

```lua
unit:event('技能-获得', function (trg, unit, skill)
end)
```

#### 技能-失去

* 参数
    + unit (unit) - 单位
    + skill (skill) - 失去的技能

```lua
unit:event('技能-失去', function (trg, unit, skill)
end)
```

#### 技能-属性变化

* 参数
    + skill (skill) - 技能
    + key (string) - 技能属性
    + value (number) - 属性值

```lua
skill:event('技能-属性变化', function (trg, skill, key, value)
end)
```

#### 技能-等级变化

* 参数
    + skill (skill) - 技能
    + level (integer) - 技能等级

```lua
skill:event('技能-等级变化', function (trg, skill, level)
end)
```

#### 技能-层数变化

* 参数
    + skill (skill) - 技能
    + stack (integer) - 层数

```lua
skill:event('技能-层数变化', function (trg, skill, stack)
end)
```

#### 技能-槽位变化

* 参数
    + skill (skill) - 技能

请使用[get_type]与[get_slot_id]来获取新的槽位状态。

```lua
skill:event('技能-层数变化', function (trg, skill)
end)
```

#### 技能-可用状态变化

* 参数
    + skill (skill) - 技能

请使用[is_enable]来获取新的禁用状态。

```lua
skill:event('技能-可用状态变化', function (trg, skill)
end)
```

#### 技能-学习状态变化

* 参数
    + skill (skill) - 技能

请使用[can_upgrade]来判断技能是否可学习。

```lua
skill:event('技能-学习状态变化', function (trg, skill)
end)
```

#### 技能-冷却激活

* 参数
    + skill (skill) - 技能
    + cd (number) - 剩余冷却（秒）
    + total (number) - 总冷却（秒）

```lua
skill:event('技能-冷却激活', function (trg, skill, cd, total)
end)
```

#### 技能-冷却完成

* 参数
    + skill (skill) - 技能

```lua
skill:event('技能-冷却完成', function (trg, skill)
end)
```

#### 技能-充能激活

* 参数
    + skill (skill) - 技能
    + cd (number) - 剩余充能冷却（秒）
    + total (number) - 总充能冷却（秒）

```lua
skill:event('技能-充能激活', function (trg, skill, cd, total)
end)
```

#### 技能-充能完成

* 参数
    + skill (skill) - 技能

```lua
skill:event('技能-充能完成', function (trg, skill)
end)
```

#### 状态-获得

* 参数
    + unit (unit) - 单位
    + buff (buff) - 获得的状态

```lua
unit:event('状态-获得', function (trg, unit, buff)
end)
```

#### 状态-失去

* 参数
    + unit (unit) - 单位
    + buff (buff) - 失去的状态

```lua
unit:event('状态-失去', function (trg, unit, buff)
end)
```

#### 状态-层数变化

* 参数
    + buff (buff) - 状态
    + stack (integer) - 层数

```lua
buff:event('状态-层数变化', function (trg, buff, stack)
end)
```

#### 玩家-改变英雄

* 参数
    + player (player) - 玩家
    + hero (unit) - 英雄

服务器使用[set_hero]修改玩家的英雄后触发。

```lua
player:event('玩家-改变英雄', function (trg, player, hero)
end)
```

#### 玩家-改变队伍

* 参数
    + player (player) - 玩家
    + team (team) - 队伍

```lua
player:event('玩家-改变队伍', function (trg, player, team)
end)
```

#### 玩家-属性变化

* 参数
    + player (player) - 玩家
    + key (string) - [玩家属性]
    + value (number) - 属性值

[玩家属性]不包括`英雄ID`、`英雄类型`和`队伍`。

```lua
player:event('玩家-属性变化', function (trg, player, key, value)
end)
```

#### 游戏-更新

* 参数
    + delta (number) - 经过的时间（毫秒）

每帧触发一次。

```lua
ac.game:event('游戏-更新', function (trg, delta)
end)
```

#### 消息-技能

* 参数
    + msg (string) - 技能消息

技能施放失败后用于显示给用户看的技能消息。

```lua
ac.game:event('消息-技能', function (trg, msg)
end)
```

#### 消息-错误

* 参数
    + msg (string) - 错误消息
    + time (number) - 持续时间（毫秒）

用于显示给用户看的错误消息。

```lua
ac.game:event('消息-错误', function (trg, msg, time)
end)
```

#### 消息-公告

* 参数
    + msg (string) - 公告消息
    + time (number) - 持续时间（毫秒）

用于显示给用户看的公告消息。

```lua
ac.game:event('消息-公告', function (trg, msg, time)
end)
```

#### 画面-分辨率变化

* 参数
    + width (integer) - 宽度
    + height (integer) - 高度

```lua
ac.game:event('画面-分辨率变化', function (trg, width, height)
end)
```

#### 按键-按下

* 参数
    + key (string) - 按键名

按键名使用`"Ctrl"`来判断任意一个Ctrl键，使用`"LeftCtrl"`与`"RightCtrl"`来精确判断某一个Ctrl键。Alt键与Shift键同理。

```lua
ac.game:event('按键-按下', function (trg, key)
end)
```

#### 按键-松开

按键名使用`"Ctrl"`来判断任意一个Ctrl键，使用`"LeftCtrl"`与`"RightCtrl"`来精确判断某一个Ctrl键。Alt键与Shift键同理。

* 参数
    + key (string) - 按键名

```lua
ac.game:event('按键-松开', function (trg, key)
end)
```

#### 鼠标-按下

* 参数
    + button (string) - 会是以下值中的一个：
        + `left` - 左键
        + `right` - 右键
        + `middle` - 中键

```lua
ac.game:event('鼠标-按下', function (trg, button)
end)
```

#### 鼠标-松开

* 参数
    + button (string) - 会是以下值中的一个：
        + `left` - 左键
        + `right` - 右键
        + `middle` - 中键

```lua
ac.game:event('鼠标-松开', function (trg, button)
end)
```

#### 鼠标-移动

当鼠标移动时，每帧触发一次。

```lua
ac.game:event('鼠标-移动', function (trg)
end)
```

[触发器]: /ac/trigger
[控制者]: http://192.168.1.19/server/#/ac/api/player?id=get_owner
[set_hero]: http://192.168.1.19/server/#/ac/api/player?id=set_hero
[单位属性]: http://192.168.1.19/server/#/ac/unit/attribute
[玩家属性]: http://192.168.1.19/server/#/ac/player/attribute
[event_notify]: /ac/event?id=event_notify
[event_dispatch]: /ac/event?id=event_dispatch
[get_type]: /ac/skill?id=get_type
[get_slot_id]: /ac/skill?id=get_slot_id
[is_enable]: /ac/skill?id=is_enable
[can_upgrade]: /ac/skill?id=can_upgrade
