# 技能

### 属性
你可以直接从技能中读取到当前的属性

```lua
local cd = skill.cool
```

### 方法
#### can_upgrade
是否可升级

* 返回
    * result (boolean) - 是否可升级

```lua
local res = skill:can_upgrade()
```

#### cast
请求使用技能

只能使用当前英雄的技能。

* 参数
    * smart (boolean) - 是否是智能施法
* 返回
    * valid (boolean) - 请求是否合法

```lua
local res = skill:cast(true)
```

#### event
注册事件

* 参数
    * name (string) - 事件名
    * callback (function) - 事件函数
* 返回
    * trigger (trigger) - 触发器
* 事件参数
    * trigger (trigger) - 触发器
    * ... (...) - 自定义数据

这是对`ac.event_register`方法的封装，你可以在[这里][event]看到详细说明。

```lua
local trigger = skill:event('技能-属性变化', function (trigger, skill, key, value)
    -- 你的代码
end)
```

#### event_notify
触发事件

* 参数
    * name (string) - 事件名
    * ... (...) - 自定义数据

这是对`ac.event_notify`方法的封装，你可以在[这里][event]看到详细说明。

```lua
skill:event_notify('自定义事件', ...)
```

#### get_charge_cd
获取充能冷却

* 返回
    * cd (number) - 剩余冷却（秒）
    * total (number) - 总冷却（秒）

如果技能并不在冷却状态，则总冷却为`0`。

```lua
local cd, total = skill:get_charge_cd()
```

#### get_cd
获取冷却

* 返回
    * cd (number) - 剩余冷却（秒）
    * total (number) - 总冷却（秒）

如果技能并不在冷却状态，则总冷却为`0`。

```lua
local cd, total = skill:get_cd()
```

#### get_level
获取等级

* 返回
    * level (integer) - 技能等级

```lua
local level = skill:get_level()
```

#### get_name
获取技能名

* 返回
    * name (string) - 技能名

如果并不知道技能名，则返回空字符串。

```lua
local name = skill:get_name()
```

#### get_slot_id
获取格子ID

* 返回
    * id (integer) - 格子ID

如果并不知道技能的格子ID，则返回`-1`。

```lua
local id = skill:get_slot_id()
```

#### get_owner
获取所有者

* 返回
    * owner (unit) - 所有者

如果没有所有者，则返回`nil`。

```lua
local owner = skill:get_owner()
```

#### get_stack
获取层数

* 返回
    * stack (integer) - 层数

```lua
local stack = skill:get_stack()
```

#### get_tip
获取技能描述

* 返回
    * tip (string) - 技能描述（`Description`）
    * short (string) - 短描述（`ShortDes`）
    * upgrade (string) - 升级描述（`UpgradeDes`）

返回当前等级经过计算后的描述。

```lua
local tip, short, upgrade = skill:get_tip()
```

#### get_type
获取类型

* 返回
    * type (string) - [技能类型]

如果并不知道技能的类型，则返回`"未知"`。

```lua
local type = skill:get_type()
```

#### hide_range
隐藏施法范围

* 返回
    * valid (boolean) - 是否合法

指示器默认不跟随鼠标移动。

```lua
local res = skill:show_range(true)
```

#### is_enable
是否可用

* 返回
    * result (boolean) - 是否可用

```lua
local result = skill:is_enable()
```

#### move
移动

* 参数
    * slot (integer) - 目标槽位
* 返回
    * valid (boolean) - 是否合法

只能移动类型为`物品`的技能。

```lua
local res = skill:move(2)
```

#### show_range
显示施法范围

* 参数
    * *follow* (boolean) - 指示器是否跟随鼠标
* 返回
    * valid (boolean) - 是否合法

指示器默认不跟随鼠标移动。

```lua
local res = skill:show_range(true)
```

#### upgrade
请求升级

* 返回
    * valid (boolean) - 是否合法

只能升级[可升级][can_upgrade]的技能

[event]: /ac/event
[can_upgrade]: /ac/skill?id=can_upgrade
[技能类型]: http://192.168.1.19/server/#/ac/skill/技能类型
