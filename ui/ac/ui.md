# 界面

幻想全明星的界面系统只提供了很有限的方法，因为大部分界面操作都可以通过[绑定]完成。

#### create
创建控件

* 参数
    * template (template) - [界面定义]
    * *name* (string) - 命名
* 返回
    * ui (ui) - 控件
    * bind (bind) - [绑定]

命名用于[服务器修改界面]。

```lua
local ui, bind = ac.ui.create(template, '背包')
```

#### add_child
添加子控件

* 参数
    * child (ui) - 子控件
* 返回
    * result (boolean) - 是否成功

将一个控件添加为另一个控件的子控件。

```lua
parent:add_child(child)
```

#### on_tick
帧事件

* 参数
    * callback (function) - 回调函数
* 回调参数
    * delta (number) - 经过的时间（毫秒）
* 返回
    * destructor (function) - 销毁器

给控件注册回调函数，每帧都会调用这个函数。一个控件可以注册多个回调函数，当控件被移除后回调函数不再运行。你也可以主动调用销毁器来使这个回调函数不再运行。

```lua
local destroy
local total = 0
destroy = ui:on_tick(function (delta)
    total = total + delta
    if total > 1000 then
        destroy()
    end
end)
```

#### remove
移除

* 返回
    * result (boolean) - 是否成功

移除一个控件以及它的所有子控件。

```lua
ui:remove()
```

[绑定]: /ui/bind
[界面定义]: /ui/
[服务器修改界面]: http://192.168.1.19/server/#/ac/api/ui
