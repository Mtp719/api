# 游戏

#### chat
发送聊天

* 参数
    * target (string) - 聊天对象
        + 全体 ： 所有玩家都能看到聊天内容
        + 队伍 ： 只有同一个队伍的玩家能看到聊天内容
    * msg (string) - 聊天内容

```lua
ac.game:chat('队伍', '大招准备就绪！')
```

#### exit
退出游戏

```lua
ac.game:exit()
```

#### input_mouse
获取鼠标位置

* 返回
    * point (point) - 位置

```lua
local point = ac.game:input_mouse()
```

#### key_state
获取按键状态

* 参数
    * key (string) - 按键名
* 返回
    * state (boolean) - 是否按下

按键名使用`"Ctrl"`来判断任意一个Ctrl键，使用`"LeftCtrl"`与`"RightCtrl"`来精确判断某一个Ctrl键。Alt键与Shift键同理。

```lua
local state = ac.game:key_state 'Ctrl'
```

#### selected_unit
获取选中的单位

* 返回
    * unit (unit) - 选中的单位

如果没有选中单位，则返回`nil`。

```lua
local unit = ac.game:selected_unit()
```

#### show_timer
获取显示时间

* 返回
    * time (number) - 显示时间（秒）

负数表示处于倒计时状态。

```lua
local time = ac.game:show_timer()
```
