# 表
你可以使用这个库来读表。

```lua
local unit = ac.table.unit['香风智乃']
print(unit.UnitType) --> "英雄"
```

可以使用的表为：

* buff - `ClientBuff.ini`
* config - `config.ini`
* constant - `Constant.ini`
* cskill - `ClientSpell.ini`
* item - `ItemData.ini`
* skill - `SpellData.ini`
* unit - `UnitData.ini`
