# 画面

#### get_resolution
获取画面分辨率

* 返回
    * width (integer) - 宽度
    * height (integer) - 高度

```lua
local width, height = ac.screen:get_resolution()
```

#### input_mouse
获取鼠标位置

* 返回
    * position (position) - 位置

```lua
local position = ac.screen:input_mouse()
```
