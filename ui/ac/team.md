# 队伍

#### 创建
* 参数
    * id (integer) - 队伍ID
* 返回
    * team (team) - 队伍

```lua
local team = ac.team(1)
```

#### get_id
获取队伍ID

* 返回
    * id (integer) - 队伍ID

```lua
local id = team:get_id()
```

#### each_player
遍历队伍中的玩家

* 遍历
    * player (player) - 遍历到的玩家

```lua
for player in team:each_player() do
    -- player为遍历到的玩家
end
```
