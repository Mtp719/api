# 颜色

- 颜色可以使用一个字符串来描述，支持16进制表示法或rgb表示法，例如 `rgb(255, 0, 0)` 和 `#FF0000` 代表红色，`rgb(0, 255, 0)` 和 `#00FF00` 代表绿色。
- 所有颜色都可以由红、绿、蓝三种颜色混合而成，每种颜色取值范围为 0 ~ 255 (16进制表示法对应为 00 ~ FF)。
- 颜色可以附加透明度，用 rgba 来替换 rgb，最后一个参数代表颜色的透明度，取值范围 0 ~ 1。例如，`rgba(255, 0, 0, 0.5)` 代表透明度为 0.5 的红色。
- 取色工具：
<iframe width="100%" height="500" src="//jsfiddle.net/jqwidgets/cHD9a/embedded/result/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>
