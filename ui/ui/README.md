# 界面 {docsify-ignore-all}

幻想全明星的界面包含3大块功能：界面定义、排版与数据绑定。

#### 定义

定义一个大小为600*100的背包

>定义背包：

```lua
local bag = ac.ui.panel {
    layout = {
        width = 600,
        height = 100,
    },
    image = '背包.png',
}
```

> 创建控件：

```lua
local ui = ac.ui.create(bag)
```

#### 子控件

在背包中放入物品

> 将子控件定义在父控件内部：

```lua
local bag = ac.ui.panel {
    layout = {
        width = 600,
        height = 100,
    },
    image = '背包.png',

    ac.ui.label {
        layout = {
            width = 100,
            height = 100,
        },
        image = '物品.png',
    },
}
```

> 因为是lua语法，你可以分段定义：

```lua
local item = ac.ui.label {
    layout = {
        width = 100,
        height = 100,
    },
    image = '物品.png',
}

local bag = ac.ui.panel {
    layout = {
        width = 600,
        height = 100,
    },
    image = '背包.png',

    item, item, item, item, item, item,
}
```

> 这样也是可以的：

```lua
local bag = ac.ui.panel {
    layout = {
        width = 600,
        height = 100,
    },
    image = '背包.png',
}

for i = 1, 6 do
    bag[i] = ac.ui.label {
        layout = {
            width = 100,
            height = 100,
        },
        image = '物品.png',
    }
end
```

当你使用`ac.ui.create`创建`bag`时，它的内部子控件也会被一并创建。

#### 阵列

> 阵列属性可以使子控件创建多次

```lua
local bag = ac.ui.panel {
    layout = {
        width = 600,
        height = 100,
    },
    image = '背包.png',
    -- 子控件创建6次
    array = 6,

    ac.ui.label {
        layout = {
            width = 100,
            height = 100,
        },
        image = '物品.png',
    },
}
```

该功能主要用于动态控制子控件数量，这会在后文讲到

#### 排版

排版可以控制控件相对于父控件的位置，这在一个控件拥有多个子控件时非常有用

> 控制自己的排版

```lua
local bag = ac.ui.panel {
    layout = {
        width = 600,
        height = 100,
        -- 横排居右
        row_self = 'end',
        -- 竖排居中
        col_self = 'center',
    },
    image = '背包.png',
}
```

> 控制子控件的排版

```lua
local bag = ac.ui.panel {
    layout = {
        width = 600,
        height = 100,
        -- 横排居右
        row_self = 'end',
        -- 竖排居中
        col_self = 'center',
        -- 子控件是横着排列的
        direction = 'row',
        -- 子控件横排居左
        row_content = 'start',
    },
    image = '背包.png',
    array = 6,

    ac.ui.label {
        layout = {
            width = 100,
            height = 100,
        },
        image = '物品.png',
    },
}
```

#### 数据绑定

> 动态修改排版

```lua
local bag = ac.ui.panel {
    layout = {
        width = 100,
        height = 100,
    },
    bind = {
        layout = {
            width = 'width',
        },
    },
    image = '背包.png',
}
```

```lua
local ui, bind = ac.ui.create(bag)
-- 此时背包的宽度为100
bind.width = 600
-- 此时背包的宽度为600
```

> 动态修改阵列大小

```lua
local bag = ac.ui.panel {
    layout = {
        width = 600,
        height = 100,
    },
    bind = {
        array = 'count',
    },
    image = '背包.png',
    array = 1,

    ac.ui.label {
        layout = {
            width = 100,
            height = 100,
        },
        image = '物品.png',
    },
}
```

```lua
local ui, bind = ac.ui.create(bag)
-- 此时物品数量为1
bind.array = 6
-- 此时物品数量为6
```

> 命名规则

```lua
local bag = ac.ui.panel {
    layout = {
        width = 600,
        height = 100,
    },
    bind = {
        layout = {
            height = 'height',
        },
        image = 'bag.image',
    },
    image = '背包.png',

    ac.ui.label {
        layout = {
            width = 100,
            height = 100,
        },
        bind = {
            layout = {
                height = 'height',
            },
            image = 'item.image',
        },
        image = '物品.png',
    },
}
```

```lua
local ui, bind = ac.ui.create(bag)
-- 同时修改背包和物品的高度
bind.height = 200
-- 分别修改背包和物品的图案
bind.bag.image = '高级背包.png'
bind.item.image = '药水.png'
```

> 穿透阵列

```lua
local bag = ac.ui.panel {
    layout = {
        width = 600,
        height = 100,
    },
    image = '背包.png',
    array = 6,

    ac.ui.label {
        layout = {
            width = 100,
            height = 100,
        },
        bind = {
            image = 'item.image',
        },
        image = '物品.png',
    },
}
```

```lua
local ui, bind = ac.ui.create(bag)
bind.item.image[1] = '生命药水.png'
bind.item.image[2] = '魔法药水.png'
```

> 穿透多级阵列

```lua
local bag = ac.ui.panel {
    layout = {
        width = 600,
        height = 600,
    },
    image = '背包.png',
    array = 6,

    ac.ui.label {
        layout = {
            width = 100,
            height = 600,
        },
        array = 6,

        ac.ui.label {
            layout = {
                width = 100,
                height = 100,
            },
            bind = {
                image = 'item.image',
            },
            image = '物品.png',
        }
    },
}
```

```lua
local ui, bind = ac.ui.create(bag)
bind.item.image[1][1] = '生命药水.png'
bind.item.image[1][2] = '魔法药水.png'
bind.item.image[6][6] = '炉石.png'
```

#### 事件

> 注册事件

```lua
local button = ac.ui.button {
    layout = {
        width = 100,
        height = 100,
    },
    event = {
        on_click = function ()
            print '按钮被点击'
        end,
    },
    image = '按钮.png',
}
```

!> 虽然这里演示了一种注册事件的方式，但请不要这样注册事件。因为这会使你界面定义的代码与逻辑代码混在一起。正确的方式是使用绑定：

> 通过绑定注册事件

```lua
local button = ac.ui.button {
    layout = {
        width = 100,
        height = 100,
    },
    bind = {
        event = {
            on_click = 'on_click',
        },
    },
    image = '按钮.png',
}
```

```lua
local ui, bind = ac.ui.create(button)

bind.on_click = function ()
    print '按钮被点击'
end
```
