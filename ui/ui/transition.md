# 平滑

当控件的部分属性被修改后，控件会平滑过渡到新的状态。

```lua
transition = {
    -- 当宽度发生变化时，在200毫秒内平滑过渡到新的状态，使用的曲线函数为'linear'
    width = {
        time = 200,
        func = 'linear',
    },
    -- 也可以省略为这样，此时曲线函数会使用默认值'linear'
    height = 200,
}
```

`transition`是一张表，其中以键值对的方式存放了要平滑的属性以及平滑方式。平滑方式可以是一张表，其中的`time`字段表示平滑时间（毫秒），`func`字段表示使用的曲线函数；平滑方式也可以是一个数字，表示平滑时间（毫秒），此时曲线函数为`linear`。

#### 支持的属性

* width
* height
* position
* show

#### 支持的曲线函数

* linear
* ease
* ease_in
* ease_out
* ease_in_out
