# 绑定

绑定功能可以使你方便的动态修改一套界面的[属性]。更重要的是，它可以让你的界面定义与界面逻辑代码分离，使得你的代码更加清晰。

> 如何绑定一个属性

在界面定义中使用`bind`属性，将要绑定的属性赋值为一个字符串即可。

```lua
local panel = ac.ui.panel {
    image = '图片.png',
    bind = {
        image = 'icon', -- 将image属性绑定在icon中
    },
}
```

在创建ui时，会返回2个对象，第二个对象即为绑定对象。

```lua
local ui, bind = ac.ui.create(panel)
```

修改`bind`中的数据，即可修改绑定的控件属性。

```lua
bind.icon = '另一张图片.png'
```

> 绑定命名

绑定使用的命名必须符合lua语法，允许包含索引。以下都是合法的命名：

```lua
'icon'
'item.icon'
'item["图标"]'
'item[1]'
```

> 穿透阵列

由于array属性会使子控件复制多份，在使用这些绑定时你需要在后面加上索引以区分操作对象：

```lua
local bag = ac.ui.panel {
    array = 10,

    ac.ui.panel {
        array = 10,

        ac.ui.label {
            bind = {
                text = 'text',
            },
        },
    },
}
```

```lua
local ui, bind = ac.ui.create(bag)
for x = 1, 10 do
    for y = 1, 10 do
        bind.text[x][y] = ('这是第[%s]列第[%s]行的物品'):format(x, y)
    end
end
```

> 绑定结构

绑定时使用的结构与属性结构相同

```lua
local bag = ac.ui.panel {
    layout = {
        height = 0,
    },
    bind = {
        layout = {
            height = 'height',
        },
    },
}
```

绑定目标必须是一个具体的属性，目前不支持直接绑定整个结构：

```lua
local bag = ac.ui.panel {
    layout = {
        height = 0,
    },
    bind = {
        layout = 'layout', -- 这是错误的写法
    },
}
```

> 同时绑定多个属性

你可以将多个属性绑定在同一个对象上

```lua
local square = ac.ui.panel {
    bind = {
        layout = {
            width = 'length',
            height = 'length',
        },
    },
}
```

```lua
local ui, bind = ac.ui.create(square)
-- 同时修改宽和高
bind.length = 100
```

!> 注意，同时绑定多个对象时，如果他们拥有各不相同的初始值，那么读取绑定时的行为是未定义的

```lua
local panel = ac.ui.panel {
    layout = {
        width = 100,
        height = 200,
    },
    bind = {
        layout = {
            width = 'length',
            height = 'length',
        },
    },
}
```

```lua
local ui, bind = ac.ui.create(panel)
local len = bind.length --> 不保证这里读取到的值是多少
bind.length = 500
local len = bind.length --> 500
```

> 重载绑定

当定义一个复杂的界面时，你可能会复用一些子控件定义。我们来定义一个面板，他包含2个几乎一模一样的进度条：生命进度条和魔法进度条。

```lua
local bar = ac.ui.progress {
    name = '进度条',
    layout = {
        grow_width = 1.0,
        grow_height = 0.5,
    },
    bind = {
        image = 'image',
    },
}

local panel = ac.ui.panel {
    layout = {
        width = 300,
        height = 100,
    },
    -- 生命进度条
    bar,
    -- 魔法进度条
    bar,
}

local ui, bind = ac.ui.create(panel)

bind.image = '生命进度条.png'
bind.image = '魔法进度条.png'
```

由于绑定是在定义时进行的，因此生命进度条与魔法进度条的`image`属性被绑定到了同一个变量上。使用重载绑定可以解决这个问题：

```lua
local bar = ac.ui.progress {
    name = '进度条',
    layout = {
        grow_width = 1.0,
        grow_height = 0.5,
    },
}

local panel = ac.ui.panel {
    layout = {
        width = 300,
        height = 100,
    },
    -- 生命进度条
    bar >> {
        image = 'life.image'
    },
    -- 魔法进度条
    bar >> {
        image = 'mana.image'
    },
}

local ui, bind = ac.ui.create(panel)

bind.life.image = '生命进度条.png'
bind.mana.image = '魔法进度条.png'
```

重载绑定可以通过`name`指定某个具体的子控件，这在有复杂嵌套的复用界面时很有用：
```lua
local bar = ac.ui.panel {
    layout = {
        grow_width = 1.0,
        grow_height = 0.5,
    },

    ac.ui.progress {
        name = '进度条',
        layout = {
            grow_width = 1.0,
            grow_height = 1.0,
        },
    },

    ac.ui.label {
        name = '文本',
        layout = {
            grow_width = 1.0,
            grow_height = 1.0,
        },
    },
}

local panel = ac.ui.panel {
    layout = {
        width = 300,
        height = 100,
    },
    -- 生命进度条
    bar <<'进度条'>> {
        image = 'life.image'
    }   <<'文本'>> {
        text = 'life.text'
    },
    -- 魔法进度条
    bar <<'进度条'>> {
        image = 'mana.image'
    }   <<'文本'>> {
        text = 'mana.text'
    },
}

local ui, bind = ac.ui.create(panel)

bind.life.image = '生命进度条.png'
bind.mana.image = '魔法进度条.png'
bind.life.text = '1000 / 1000'
bind.mana.text = '500 / 500'
```

[属性]: /ui/attribute
