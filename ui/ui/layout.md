# 排版

所有的控件类型都支持排版。排版属性填写在`layout`属性内。

```lua
ac.ui.panel {
    layout = {
        width = 100,
        height = 100,
    },
}
```

### direction

子控件排列方向。`row`表示横向排列，`col`表示竖向排列。

```lua
direction = 'row'
```

### col_content

子控件竖向排列的方式。默认为`center`。若子控件设置了有效的[col_self]，则优先使用子控件的设置。

```lua
col_content = 'start'  -- 居上
col_content = 'center' -- 居中
col_content = 'end'    -- 居下
```

### col_self

自己竖向排列的方式。如果父控件的[direction]设置为了`col`，则此属性无效。默认为`center`。

```lua
col_self = 'start'  -- 居上
col_self = 'center' -- 居中
col_self = 'end'    -- 居下
```

### height

控件的高度。

```lua
height = 100
```

### grow_height

高度成长。成长方式参考[grow_width]。

```lua
grow_height = 0.5
```

### grow_width

宽度成长。当父控件有剩余空间时，这个值会使控件宽度变宽，以占用剩余空间。`0.5`表示会占用`50%`的剩余空间。如果有多个控件想要成长且总值超过`1.0`时，会根据各自的数字比例分配剩余空间。

```lua
grow_width = 0.5
```

### margin

外边框宽度。计算自己占用的控件时，会加上外边框。这个属性有2种格式：

```lua
margin = 5 -- 4周内边框均为5像素
margin = {
    top    = 2, -- 上边框为2像素
    bottom = 1, -- 下边框为1像素
    left   = 3, -- 左边框为3像素
    right  = 0, -- 右边框为0像素
}
```

### padding

内边框宽度。计算子控件空间时，会扣除掉内边框。这个属性有2种格式：

```lua
padding = 5 -- 4周内边框均为5像素
padding = {
    top    = 2, -- 上边框为2像素
    bottom = 1, -- 下边框为1像素
    left   = 3, -- 左边框为3像素
    right  = 0, -- 右边框为0像素
}
```

### position

绝对位置。控件位置会使用此值，且不再参与排版。这可能会导致控件重叠等现象，请谨慎使用。

```lua
position = {100, 50}
```

### relative

相对位置。当排版完成后，控件位置会根据此值进行偏移。这可能会导致控件重叠等现象，请谨慎使用。

```lua
relative = {10, 5} -- 向右偏移10像素，向下偏移5像素
```

### row_content

子控件横向排列的方式。默认为`center`。若子控件设置了有效的[row_self]，则优先使用子控件的设置。

```lua
row_content = 'start'  -- 居左
row_content = 'center' -- 居中
row_content = 'end'    -- 居右
```

### row_self

自己横向排列的方式。如果父控件的[direction]设置为了`row`，则此属性无效。默认为`center`。

```lua
row_self = 'start'  -- 居左
row_self = 'center' -- 居中
row_self = 'end'    -- 居右
```

### width

控件的宽度。

```lua
width = 100
```

[grow_width]: /ui/layout?id=grow_width
[direction]: /ui/layout?id=direction
[row_self]: /ui/layout?id=row_self
[col_self]: /ui/layout?id=col_self
