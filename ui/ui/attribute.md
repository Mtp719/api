# 属性

#### active_image
激活图片

只有[button]支持该属性，用于鼠标按下按钮时按钮显示的图片。

```lua
active_image = '按下.png'
```

#### array
阵列

只有[panel]支持该属性。设置该属性后，子控件会被复制N份。

```lua
array = 5
```

#### bind
[绑定]

```lua
bind = {
    image = 'image',
}
```

#### color
背景[颜色]

```lua
color = '#ff1111'
```

#### data
数据

当其他控件拖到这个控件上时，这张表将作为参数传入[on_drop]。

```lua
data = {
    type = '物品',
    slot = 1,
}
```

#### enable
启用

设置为`false`可以让控件的事件全部失效。

```lua
enable = false
```

#### enable_drag
可被拖动

设置为`true`后，可以拖动这个控件。

```lua
enable_drag = true
```

#### enable_drop
可被放开

设置为`true`后，允许其他控件拖到这个控件上（触发[on_drop]事件）。

```lua
enable_drop = true
```

#### event
[事件]

```lua
bind = {
    event = {
        on_click = 'on_click',
    },
}
```

#### font
[字体]

只有[label]支持该属性。

```lua
font = {
    size = 10,
    color = '#fff',
}
```

#### hover_image
悬停图片

只有[button]支持该属性，用于鼠标悬停在按钮上时按钮显示的图片。

```lua
hover_image = '高亮.png'
```

#### image
背景图片

```lua
image = '图案.png'
```

#### layout
[排版]

```lua
layout = {
    width = 100,
    height = 100,
}
```

#### mask_image
遮罩图片

遮罩图片应该是一个带有透明通道的图片，控件会根据透明通道的形状进行剪裁。

```lua
mask_image = '化方为圆.png'
```

#### name
命名

这个属性没有实际的功能，但是将控件强制转换为字符串时会带上这个名字，用于调试信息。

```lua
name = '背包'
```

#### progress
进度

只有[progress]支持该属性，用于设置进度条的进度，取值范围为[0, 1]。

```lua
progress = 0.5
```

#### progress_type
进度类型

只有[progress]支持该属性，用于设置进度条的类型。

```lua
progress_type = 'left'              -- 从右往左
progress_type = 'right'             -- 从左往右
progress_type = 'up'                -- 从下往上
progress_type = 'down'              -- 从上往下
progress_type = 'clockwise'         -- 顺时针
progress_type = 'counter_clockwise' -- 逆时针
```

#### show
显示

设置为`false`可以让控件不可见，也不会参与排版。

```lua
show = false
```

#### static
静态

设置为`true`后，控件不会接收任何事件。例如界面上显示一段公告文本，此时公告文本需要置顶，但事件却要穿透文本。

```lua
static = true
```

#### swallow_event
吃掉事件

设置为`true`后，事件不会向父控件传递下去。例如背包本身有点击事件，而背包的子控件物品也有点击事件，那么你可以设置物品的`swallow_event = true`，这样点击物品后就不会触发背包的事件。

```lua
swallow_event = true
```

#### text
文本

只有[label]支持该属性。

```lua
text = '文本内容'
```

#### transition
[平滑]

```lua
transition = {
    position = 200,
}
```

#### z_index
层级

当一个控件的多个子控件互相重叠时，`z_index`较大的子控件会在上面。

```lua
z_index = 1
```

[排版]: /ui/layout
[绑定]: /ui/bind
[事件]: /ui/event
[平滑]: /ui/transition
[颜色]: /ui/color
[字体]: /ui/font

[on_drop]: /ui/event?id=on_drop
[data]: /ui/attribute?id=data

[panel]: /ac/template?id=panel
[label]: /ac/template?id=label
[button]: /ac/template?id=button
[progress]: /ac/template?id=progress
