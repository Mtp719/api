# 字体

字体用于描述一段文本的样式。

```lua
font = {
    size = 20,
    color = '#ff1111',
}
```

#### align
横向对齐方式

```lua
align = 'left'   -- 左对齐
align = 'center' -- 居中
align = 'right'  -- 右对齐
```

#### bold
加粗

```lua
bold = true
```

#### border
描边

这是一个由2段字符组成的字符串，`'2px #000000'`表示描边粗细为2像素，使用[颜色]为`#000000`。

```lua
border = '2px #000000'
```

#### color
字体的[颜色]。

```lua
color = '#FF0000'
```

#### family
字体系列，目前支持两种字体: 微软雅黑`Microsoft Yahei` 和宋体 `SimSun`

```lua
family = 'Microsoft Yahei'
```

#### line_height
行高

当文本显示为多行时，这个参数代表每行文本的高度，单位为字体大小的倍数。

下面这一句代码代表行高为字体大小的 1.2 倍，也可以理解为行距是字体大小的 0.2 倍。（行距 = 行高 - 1）

```lua
line_height = 1.2
```

#### shadow
阴影

这是一个由4段字符组成的字符串，`'2px 3px 4px #000000'`表示阴影向右偏移2像素，向下偏移3像素，模糊4像素，使用[颜色]为`#000000`。

```lua
shadow = '2px 2px 2px #000000'
```

#### size
字体大小

```lua
size = 20
```

#### vertical_align
纵向对齐方式

```lua
vertical_align = 'top'    -- 上对齐
vertical_align = 'center' -- 居中
vertical_align = 'bottom' -- 下对齐
```

[颜色]: /ui/color
