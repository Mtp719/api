# 事件

你可以利用[绑定]为控件添加事件

```lua
local panel = ac.ui.panel {
    array = 10,

    ac.ui.button {
        bind = {
            event = {
                on_click = 'on_click',
            },
        },
    },
}
```

```lua
local ui, bind = ac.ui.create(panel)
for i = 1, 10 do
    bind.on_click[i] = function ()
        print(('第[%s]个按钮被点击'):format(i))
    end
end
```

下面会介绍所有的事件，为了简便起见，例子里假定所有的事件名与绑定名是相同的。

### on_click

单击

```lua
bind.on_click = function ()
end
```

### on_double_click

双击

```lua
bind.on_double_click = function ()
end
```

### on_drag

拖动

当开始拖动控件时触发。

```lua
bind.on_drag = function ()
end
```

### on_drop

放开

* 参数
    * data (table) - 另一个控件的[data]属性

当一个控件拖动到另一个控件上时触发。该`data`属性会作为参数传入事件，你可以通过定制与判断`data`属性的内容来区分你将控件拖动到了哪儿。

```lua
bind.on_drop = function (data)
    -- data是另一个控件的drop属性
    if data.type ~= '物品' then
        return
    end
end
```

### on_mouse_enter

鼠标进入

```lua
bind.on_mouse_enter = function ()
end
```

### on_mouse_leave

鼠标离开

```lua
bind.on_mouse_leave = function ()
end
```

### on_throw

丢弃

* 参数
    * point (point) - 丢弃的位置

当一个控件拖动到没有合法控件的位置上时触发。此时该位置所对应的游戏位置会作为参数传入。

```lua
bind.on_throw = function (point)
end
```

[绑定]: /ac/ui/bind
[data]: /ui/attribute?id=data
