# 物品
物品的说明见[这里][物品]。

[物品]: /ac/item

#### blink
传送

* 参数
    * target (point) - 目标位置
* 返回
    * result (boolean) - 是否成功

如果物品被单位持有，那么单位会去该物品

```lua
item:blink(target)
```

#### get_holder
获取持有者

* 返回
    * holder (unit) - 持有者

如果单位不被任何单位持有，则返回 `nil` 。

```lua
local holder = item:get_holder()
```

#### get_name
获取物品名

* 返回
    * name (string) - 物品名

```lua
local name = item:get_name()
```

#### get_owner
获取所有者

* 返回
    * owner (unit/player) - 所有者

物品单位的所有者。

```lua
local owner = item:get_owner()
```

#### remove
移除

```lua
item:remove()
```


