# 表
你可以使用这个库来读表。

```lua
local unit = ac.table.unit['香风智乃']
print(unit.UnitType) --> "英雄"
```

可以使用的表为：

* attack - `CommonSpellData.ini`
* buff - `ClientBuff.ini`
* card - `CardData.ini`
* config - `config.ini`
* constant - `Constant.ini`
* item - `ItemData.ini`
* skill - `SpellData.ini`
* unit - `UnitData.ini`
