# 数学

#### acos
反余弦

* 参数
    * cos (number) - 余弦值
* 返回
    * angle (number) - 角度

```lua
local angle = ac.math.acos(0.5)
```

#### asin
反正弦

* 参数
    * sin (number) - 正弦值
* 返回
    * angle (number) - 角度

```lua
local angle = ac.math.asin(0.5)
```

#### atan
反正切

* 参数
    * y (number) - 对边
    * *x* (number) - 临边
* 返回
    * angle (number) - 角度

返回 y/x 的反正切值（用角度表示）。 它会使用两个参数的符号来找到结果落在哪个象限中。 （即使 x 为零时，也可以正确的处理。）

默认的 x 是 1 ， 因此调用 ac.math.atan(y) 将返回 y 的反正切值。

```lua
local angle = ac.math.tan(1, 2)
local angle = ac.math.tan(2)
```

#### cos
余弦

* 参数
    * angle (number) - 角度
* 返回
    * cos (number) - 余弦值

```lua
local cos = ac.math.cos(60)
```

#### included_angle
求夹角

* 参数
    * r1 (number) - 角度1
    * r2 (number) - 角度2
* 返回
    * angle (number) - 夹角
    * direction (integer) - 方向

`angle`的范围为[0, 180]，`direction`为-1或1，且满足`r1 + angle * direction == r2`。

```lua
local angle, direction = ac.math.included_angle(r1, r2)
```

#### sin
正弦

* 参数
    * angle (number) - 角度
* 返回
    * sin (number) - 正弦值

```lua
local sin = ac.math.sin(30)
```

#### tan
正切

* 参数
    * angle (number) - 角度
* 返回
    * tan (number) - 正切值

```lua
local tan = ac.math.tan(45)
```
