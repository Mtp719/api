# 界面
界面库可以利用[绑定]来修改客户端界面。

!> 以下代码在[界面]中执行

```lua
local bag = ac.ui.panel {
    layout = {
        width = 100,
        height = 100,
    },
    bind = {
        layout = {
            width = 'width',
        },
    },
    image = '背包.png',
}

local ui, bind = ac.ui.create(bag, '背包')
```

!> 以下代码在[服务器]中执行，假定对应的玩家为`player`

```lua
local bind = ac.ui.bind(player, '背包')
bind.width = 600 -- 将背包的宽度修改为600
```

需要注意的是，服务器使用的`bind`是只写的，你无法从里面读取出正确的数据。此外你每次修改数据都会产生网络流量，请谨慎使用。

除了修改数据，你也可以利用它来注册控件事件：

!> 以下代码在[界面]中执行

```lua
local button = ac.ui.button {
    layout = {
        width = 100,
        height = 100,
    },
    bind = {
        event = {
            on_click = 'on_click',
        },
    },
    image = '按钮.png',
}

local ui, bind = ac.ui.create(button, '按钮')
```

!> 以下代码在[服务器]中执行

```lua
local bind = ac.ui.bind(player, '按钮')
bind.on_click = function ()
    print '按钮被点击'
end
```

和修改属性类似，当你注册事件后每次控件触发此事件时都会产生网络流量，请谨慎使用。

#### bind
创建绑定

* 参数
    * player (player) - 玩家
    * name (string) - 绑定名
* 返回
    * bind (bind) - 绑定

绑定名指的是界面[创建控件]时制定的名字。

```lua
local bind = ac.ui.bind(player, '背包')
```

[绑定]: http://192.168.1.19/ui/#/ui/bind
[创建控件]: http://192.168.1.19/ui/#/ac/ui?id=create
[界面]: http://192.168.1.19/ui
[服务器]: http://192.168.1.19/server
